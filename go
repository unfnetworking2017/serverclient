import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {

        public static int PORT_NUMBER = 2992;
        Socket clientSocket = null;
        private PrintWriter toClient;

        Server(Socket clientSocket) {
                this.clientSocket = clientSocket;
        }

        @SuppressWarnings("resource")
        public static void main(String[] args) throws IOException {
                System.out.println("Executing Server Process...");
                // Create a Server Socket Object
                ServerSocket listener;
                listener = new ServerSocket(PORT_NUMBER);
                while (true) {
                        Socket sock = listener.accept();
                        new Thread(new Server(sock)).start();
                }

        }

        public void displayCommands() throws IOException{

        }

        @Override
        public void run(){
                System.out.println("Forked a new Server...");
                BufferedReader fromClient = null;
                int command;
                try {
                        fromClient = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
                        toClient = new PrintWriter(this.clientSocket.getOutputStream(), true);
                } catch (IOException e1) }
                }
                Process cmdProc = null;
                while (true) {
                        try {
                    while((command = Integer.parseInt(fromClient.readLine() != null){
//                                              command = Integer.parseInt(fromClient.readLine());
                                                if(command < 0 || command > 6)
                                                {
                                                        toClient.println("Invalid Command! - Press Enter to continue..");
                                                }
                                                switch(command)
                                                {
                                                case 1:
                                                {
                                                cmdProc = Runtime.getRuntime().exec("date -r");
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println("Test Message 1");
                                                        }
                                                        break;
                                                }
                                                case 2:
                                                {
                                                        cmdProc = Runtime.getRuntime().exec("uptime");
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println(cmdans);
                                                        }
                                                        break;
                                                }
                                                case 3:
                                                {
                                                        String[] cmd = {"bash", "-c", "vmstat -s|head -2"};
                                                        cmdProc = Runtime.getRuntime().exec(cmd);
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)

                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println(cmdans);
                                                        }
                                                        break;
                                                }
                                                case 4:
                                                {
                                                        cmdProc = Runtime.getRuntime().exec("netstat");
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println(cmdans);
                                                        }
                                                        break;
                                                }
                                                case 5:
                                                {
                                                        cmdProc = Runtime.getRuntime().exec("users");
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println(cmdans);
                                                        }
                                                        break;
                                                }
                                                case 6:
                                                {
                                                        String[] cmd = {"bash", "-c", "cat /etc/issue|head -1"};
                                                        cmdProc = Runtime.getRuntime().exec(cmd);
                                                        BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
                                                        String cmdans;
                                                        while((cmdans = cmdin.readLine()) != null)
                                                        {
                                                                toClient.println(cmdans);
                                                        }
                                                        break;
                                                }
                                        }
                                        System.out.println("Client Replied response "+command);

                                        }
                                        System.out.println("Client Replied response "+command);
                                        //break;
                                }
                        } catch (IOException e) {
                        }
                }
        }
}