import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {

	public static int PORT_NUMBER = 2992;
	Socket clientSocket = null;

	public static String [] commands = {"Host current Date and Time","Host uptime","Host memory use",
			"Host IPV4 socket connections","Host current users","Show current OS version","Quit"};
	private PrintWriter out;

	Server(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		System.out.println("Executing Server Process..");
		// Create a Server Socket Object
		ServerSocket listener;
		listener = new ServerSocket(PORT_NUMBER);
		while (true) {
			Socket sock = listener.accept();
			new Thread(new Server(sock)).start();
		}

	}

	public void displayCommands() throws IOException{
	
	}	
	
	@Override
	public void run(){
		System.out.println("Forked a new Server...");
		BufferedReader input = null;
		try {
			input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
			out = new PrintWriter(this.clientSocket.getOutputStream(), true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Process cmdProc = null;
		while (true) {
			try {
				for(int i=0;i<commands.length;i++){
					out.println(i+1+". "+commands[i]);
				}
				out.println("Please Enter Command Number(1-7): ");
				while(!input.ready())
				{
					int command;
					String response = input.readLine();
					try{
						 command = Integer.parseInt(response);
					}catch(Exception e){
						break;
					}
					if(command<0 || command>7)
					{
						out.println("Invalid Command! - Press Enter to continue..");
					}
					switch(command)
					{
						case 1:
						{
							cmdProc = Runtime.getRuntime().exec("date -R");
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 2:
						{
							cmdProc = Runtime.getRuntime().exec("uptime");
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 3:
						{
							String[] cmd = {"bash", "-c", "vmstat -s|head -2"};
							cmdProc = Runtime.getRuntime().exec(cmd);
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 4:
						{
							String[] cmd = {"bash", "-c", "ss -tl -f inet"};
							cmdProc = Runtime.getRuntime().exec(cmd);
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 5:
						{
							cmdProc = Runtime.getRuntime().exec("users");
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 6:
						{
							String[] cmd = {"bash", "-c", "cat /etc/issue|head -1"};
							cmdProc = Runtime.getRuntime().exec(cmd);
							BufferedReader cmdin = new BufferedReader(new InputStreamReader(cmdProc.getInputStream()));
							String cmdans;
							while((cmdans = cmdin.readLine()) != null)
							{
								out.println(cmdans);
							}
							break;
						}
						case 7:
						{
							out.println("Good Bye...");
							this.clientSocket.close();
							return;
						}
					}
					System.out.println("Client Replied response"+command);
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
