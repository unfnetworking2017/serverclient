import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class average {

    public static void main(String args[])throws IOException{
        String line;
        double value = 0;
        int total = Integer.parseInt(args[0]);
        FileReader read = new FileReader("output.txt");
        BufferedReader reading = new BufferedReader(read);
        while((line = reading.readLine()) != null){
            value += Double.parseDouble(line);
        }
        value = value / total;
        System.out.println(value);
             
    }
    
}
